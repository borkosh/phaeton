﻿using System;
using System.Collections.Generic;

namespace PhaetonReport.Models
{
    public partial class UsersContragents
    {
        public UsersContragents()
        {
            OrderHistory = new HashSet<OrderHistory>();
            SearchHistory = new HashSet<SearchHistory>();
        }

        public int Id { get; set; }
        public string City { get; set; }
        public string AccountNumber { get; set; }
        public string Name { get; set; }
        public int? UserId { get; set; }

        public Users User { get; set; }
        public ICollection<OrderHistory> OrderHistory { get; set; }
        public ICollection<SearchHistory> SearchHistory { get; set; }
    }
}
