﻿using System;
using System.Collections.Generic;

namespace PhaetonReport.Models
{
    public partial class Users
    {
        public Users()
        {
            UsersContragents = new HashSet<UsersContragents>();
        }

        public int Id { get; set; }
        public string Fio { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int? UserType { get; set; }

        public ICollection<UsersContragents> UsersContragents { get; set; }
    }
}
