﻿using System;
using System.Collections.Generic;

namespace PhaetonReport.Models
{
    public partial class SearchHistory
    {
        public int Id { get; set; }
        public int? UsersContragentId { get; set; }
        public string ProductNumber { get; set; }
        public string Brand { get; set; }
        public string IpAddress { get; set; }

        public UsersContragents UsersContragent { get; set; }
    }
}
