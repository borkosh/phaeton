﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PhaetonReport.Models
{
    public partial class PhaetonContext : DbContext
    {
        public PhaetonContext()
        {
        }

        public PhaetonContext(DbContextOptions<PhaetonContext> options): base(options)
        {
        }

        public virtual DbSet<OrderHistory> OrderHistories { get; set; }
        public virtual DbSet<SearchHistory> SearchHistories { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<UsersContragents> UsersContragents { get; set; }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderHistory>(entity =>
            {
                entity.Property(e => e.Brand).HasMaxLength(50);

                entity.Property(e => e.ProductNumber).HasMaxLength(50);

                entity.HasOne(d => d.UsersContragent)
                    .WithMany(p => p.OrderHistory)
                    .HasForeignKey(d => d.UsersContragentId)
                    .HasConstraintName("FK_OrderHistory_UsersContragents");
            });

            modelBuilder.Entity<SearchHistory>(entity =>
            {
                entity.Property(e => e.Brand).HasMaxLength(50);

                entity.Property(e => e.IpAddress).HasMaxLength(50);

                entity.Property(e => e.ProductNumber).HasMaxLength(50);

                entity.HasOne(d => d.UsersContragent)
                    .WithMany(p => p.SearchHistory)
                    .HasForeignKey(d => d.UsersContragentId)
                    .HasConstraintName("FK_SearchHistory_UsersContragents");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.Fio)
                    .HasColumnName("FIO")
                    .HasMaxLength(150);

                entity.Property(e => e.Login).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(50);
            });

            modelBuilder.Entity<UsersContragents>(entity =>
            {
                entity.Property(e => e.AccountNumber).HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UsersContragents)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_UsersContragents_Users");
            });
        }
    }
}
