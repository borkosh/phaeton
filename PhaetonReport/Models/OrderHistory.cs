﻿using System;
using System.Collections.Generic;

namespace PhaetonReport.Models
{
    public partial class OrderHistory
    {
        public int Id { get; set; }
        public int? UsersContragentId { get; set; }
        public string ProductNumber { get; set; }
        public string Brand { get; set; }
        public int? Amount { get; set; }

        public UsersContragents UsersContragent { get; set; }
    }
}
