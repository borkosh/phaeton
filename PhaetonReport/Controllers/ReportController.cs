﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhaetonReport.Models;

namespace PhaetonReport.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly PhaetonContext _context;

        public ReportController(PhaetonContext context)
        {
            _context = context;
        }


        [HttpGet]
        public ActionResult Get()
        {
            var orders = _context.OrderHistories.Where(a=>a.Id<1000).ToList();

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("OrderHistories");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Id";
                worksheet.Cell(currentRow, 2).Value = "ProductNumber";
                worksheet.Cell(currentRow, 3).Value = "Brand";
                worksheet.Cell(currentRow, 4).Value = "Amount";
                foreach (var order in orders)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = order.Id;
                    worksheet.Cell(currentRow, 2).Value = order.ProductNumber;
                    worksheet.Cell(currentRow, 3).Value = order.Brand;
                    worksheet.Cell(currentRow, 4).Value = order.Amount;
                }

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "OrderHistories.xlsx");
                }
            }
        }

       
    }
}